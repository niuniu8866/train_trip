import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {

    public static void main(String[] args) {

        System.out.print("Graph : ");
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String input = null;
        try {
            input = in.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] inputArray = input.split(",");
        List<String> inputList = new ArrayList<>(Arrays.asList(inputArray));
        GraphInfo graphInfo = new GraphInfo(inputList);

        Graph graph = graphInfo.getGraph();
        HashMap<Character, Integer> cityNameMap = graphInfo.getCityNameMap();

        TripTasks.distanceForRouteTasks(1, new ArrayList<>(Arrays.asList('A','B','C')),cityNameMap,graph);
        TripTasks.distanceForRouteTasks(2,new ArrayList<>(Arrays.asList('A','D')),cityNameMap,graph);
        TripTasks.distanceForRouteTasks(3,new ArrayList<>(Arrays.asList('A','D','C')),cityNameMap,graph);
        TripTasks.distanceForRouteTasks(4,new ArrayList<>(Arrays.asList('A','E','B','C','D')),cityNameMap,graph);
        TripTasks.distanceForRouteTasks(5,new ArrayList<>(Arrays.asList('A','E','D')),cityNameMap,graph);
        TripTasks.unWeightedTripCountAtAMaxStopTasks(6,graph,cityNameMap,'C', 'C',3);
        TripTasks.unWeightedTripCountExactlyStopsTasks(7,graph,cityNameMap,'A', 'C',4);
        TripTasks.shortestDistanceTasks(8, graph, cityNameMap, 'A','C');
        TripTasks.shortestDistanceTasks(9, graph, cityNameMap,'B','B');
        TripTasks.weightedTripCountTasks(10, graph, cityNameMap,'C', 'C', 30);
    }
}
