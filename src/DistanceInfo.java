/**
 * A Class that hold the distance info of any vertex.
 * The distance here means the distance from the source vertex
 * The lastVertex here means the last vertex before current vertex
 */


public class DistanceInfo {
    private int distance;
    private int lastVertex;

    public DistanceInfo(){
        distance = Integer.MAX_VALUE;
        lastVertex = -1;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public void setLastVertex(int lastVertex) {
        this.lastVertex = lastVertex;
    }

    public int getDistance() {
        return distance;
    }

    public int getLastVertex() {
        return lastVertex;
    }
}
