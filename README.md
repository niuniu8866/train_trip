A Direct Weighted Graph Tranversal Problem

Compile:
javac AdjacencyMatrixGraph.java DistanceInfo.java GraphInfo.java Graph.java Main.java TripTasks.java utils.java VertexInfo.java

Usage:
java Main

Sample Usage:
dev@dev-ThinkPad-W510:~/directed_cyclic_graph/src$ java Main 
Graph : AB5, BC4,CD8,DC8,DE6,AD5,CE2,EB3,AE7
Output #1: 9
Output #2: 5
Output #3: 13
Output #4: 22
Output #5: NO SUCH ROUTE
Output #6: 2
Output #7: 3
Output #8: 9
Output #9: 9
Output #10: 7



Environment:
Tested with OpenJDK8 under Ubuntu 16.04LTS
