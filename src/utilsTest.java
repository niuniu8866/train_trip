import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class utilsTest {

    @org.junit.Test
    public void buildDistanceTable() {
    }

    @org.junit.Test
    public void shortestPath() {
        List<String> test = new ArrayList<>(Arrays.asList("AB5","BC4","CD8","DC8","DE6","AD5","CE2","EB3","AE7"));
        GraphInfo graphInfo = new GraphInfo(test);

        Graph graph = graphInfo.getGraph();
        HashMap<Character, Integer> cityNameMap = graphInfo.getCityNameMap();
        int expect = 9;
        int actual  = utils.shortestPath(graph,cityNameMap.get('A'), cityNameMap.get('C'));
        assertThat(actual, is(expect));
    }


    @org.junit.Test
    public void distanceOnPath() {
        List<String> test = new ArrayList<>(Arrays.asList("AB5","BC4","CD8","DC8","DE6","AD5","CE2","EB3","AE7"));
        GraphInfo graphInfo = new GraphInfo(test);

        Graph graph = graphInfo.getGraph();
        HashMap<Character, Integer> cityNameMap = graphInfo.getCityNameMap();


        List<Character> path = new ArrayList<>(Arrays.asList('A', 'B','C'));

        int expect = 9;
        int actual = utils.distanceOnPath(graph,utils.generatePathList(cityNameMap,path));
        assertThat(actual, is(expect));
    }

    @org.junit.Test
    public void distanceOnPathInvalidNode() {
        List<String> test = new ArrayList<>(Arrays.asList("AB5","BA2"));
        GraphInfo graphInfo = new GraphInfo(test);

        Graph graph = graphInfo.getGraph();
        HashMap<Character, Integer> cityNameMap = graphInfo.getCityNameMap();


        List<Character> path = new ArrayList<>(Arrays.asList('A', 'B','C'));

        int expect = 0;
        int actual = utils.distanceOnPath(graph,utils.generatePathList(cityNameMap,path));
        assertThat(actual, is(expect));
    }


    @org.junit.Test
    public void generatePathList() {
        List<String> test = new ArrayList<>(Arrays.asList("AB5","BC4","CD8","DC8","DE6","AD5","CE2","EB3","AE7"));
        GraphInfo graphInfo = new GraphInfo(test);

        Graph graph = graphInfo.getGraph();
        HashMap<Character, Integer> cityNameMap = graphInfo.getCityNameMap();

        List<Character> path = new ArrayList<>(Arrays.asList('A', 'B','C'));
        List<Integer> expect = new ArrayList<>(Arrays.asList(0,1,2));
        List<Integer> actual = utils.generatePathList(cityNameMap,path);
        assertThat(actual, is(expect));
    }

    @org.junit.Test
    public void bfsWithSrcAndDestUnweighted() {
        List<String> test = new ArrayList<>(Arrays.asList("AB5","BC4","CD8","DC8","DE6","AD5","CE2","EB3","AE7"));
        GraphInfo graphInfo = new GraphInfo(test);

        Graph graph = graphInfo.getGraph();
        HashMap<Character, Integer> cityNameMap = graphInfo.getCityNameMap();
        int actual = utils.bfsWithSrcAndDestUnweighted(graph,cityNameMap.get('C'),cityNameMap.get('C'),3);
        int expect = 1;
        assertThat(actual, is(expect));
    }

    @org.junit.Test
    public void bfsWithSrcAndDestWeighted() {
        List<String> test = new ArrayList<>(Arrays.asList("AB5","BC4","CD8","DC8","DE6","AD5","CE2","EB3","AE7"));
        GraphInfo graphInfo = new GraphInfo(test);

        Graph graph = graphInfo.getGraph();
        HashMap<Character, Integer> cityNameMap = graphInfo.getCityNameMap();
        int actual = utils.bfsWithSrcAndDestWeighted(graph,cityNameMap.get('C'),cityNameMap.get('C'),30);
        int expect = 7;
        assertThat(actual, is(expect));
    }
}