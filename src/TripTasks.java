import java.util.HashMap;
import java.util.List;

public class TripTasks {

    public static void distanceForRouteTasks(int taskId, List<Character> taskInput,
                                             HashMap<Character, Integer> cityNameMap, Graph graph){
        System.out.print("Output #" + taskId +": ");
        int taskOutput = utils.distanceOnPath(graph,utils.generatePathList(cityNameMap,taskInput));
        if (taskOutput == 0){
            System.out.println("NO SUCH ROUTE");
        } else {
            System.out.println(taskOutput);
        }
    }

    public static void unWeightedTripCountExactlyStopsTasks(int taskId, Graph graph,
                                                            HashMap<Character, Integer> cityNameMap,
                                                            char source, char dest, int numsStop ){
        System.out.print("Output #" + taskId +": ");
        if (!cityNameMap.containsKey(source) || !cityNameMap.containsKey(dest)){
            System.out.println("NO SUCH ROUTE");
            return;
        }
        int taskOutput = utils.bfsWithSrcAndDestUnweighted(graph,cityNameMap.get(source),cityNameMap.get(dest),numsStop);
        if (taskOutput == 0){
            System.out.println("NO SUCH ROUTE");
        } else {
            System.out.println(taskOutput);
        }
    }

    public static void unWeightedTripCountAtAMaxStopTasks(int taskId, Graph graph,
                                                          HashMap<Character, Integer> cityNameMap,
                                                            char source, char dest, int maxStop ){
        System.out.print("Output #" + taskId +": ");
        if (!cityNameMap.containsKey(source) || !cityNameMap.containsKey(dest)){
            System.out.println("NO SUCH ROUTE");
            return;
        }
        int taskOutput = 0;
        for (int i =2; i <= maxStop; i++){
            taskOutput +=utils.bfsWithSrcAndDestUnweighted(graph,cityNameMap.get(source),cityNameMap.get(dest),i);
        }
        if (taskOutput == 0){
            System.out.println("NO SUCH ROUTE");
        } else {
            System.out.println(taskOutput);
        }
    }

    public static void shortestDistanceTasks(int taskId, Graph graph,HashMap<Character, Integer> cityNameMap,
                                             char source, char dest){
        System.out.print("Output #" + taskId +": ");
        if (!cityNameMap.containsKey(source) || !cityNameMap.containsKey(dest)){
            System.out.println("NO SUCH ROUTE");
            return;
        }
        int taskOutput = utils.shortestPath(graph,cityNameMap.get(source),cityNameMap.get(dest));
        if (taskOutput == 0){
            System.out.println("NO SUCH ROUTE");
        } else {
            System.out.println(taskOutput);
        }
    }

    public static void weightedTripCountTasks(int taskId, Graph graph,HashMap<Character, Integer> cityNameMap,
                                              char source, char dest, int weight){
        System.out.print("Output #" + taskId +": ");
        if (!cityNameMap.containsKey(source) || !cityNameMap.containsKey(dest)){
            System.out.println("NO SUCH ROUTE");
            return;
        }
        int taskOutput = utils.bfsWithSrcAndDestWeighted(graph,cityNameMap.get(source),cityNameMap.get(dest),weight);
        if (taskOutput == 0){
            System.out.println("NO SUCH ROUTE");
        } else {
            System.out.println(taskOutput);
        }
    }
}
