import java.util.*;

/**
 * Utils that does the true work
 */
public class utils {

    /**
     * build the distance Table for Dijkstra Algorithm
      * @param graph
     * @param source
     * @return
     */
public static Map<Integer, DistanceInfo> buildDistanceTable(Graph graph, int source){
    Map<Integer, DistanceInfo> distanceTable = new HashMap<>();
    PriorityQueue<VertexInfo> queue = new PriorityQueue<>(new Comparator<VertexInfo>() {
        @Override
        public int compare(VertexInfo o1, VertexInfo o2) {
            return ((Integer)o1.getDistance()).compareTo(o2.getDistance());
        }
    });

    Map<Integer, VertexInfo> vertexInfoMap = new HashMap<>();

    for( int i = 0 ; i < graph.getNumVerticess(); i++){
        distanceTable.put(i, new DistanceInfo());
    }

    distanceTable.get(source).setDistance(0);
    distanceTable.get(source).setLastVertex(source);

    VertexInfo sourceVertexInfo = new VertexInfo(source, 0);
    queue.add(sourceVertexInfo);
    vertexInfoMap.put(source, sourceVertexInfo);

    while (!queue.isEmpty()){
        VertexInfo vertexInfo = queue.poll();
        int currentVertex = vertexInfo.getVertexId();

        for (Integer neighbour : graph.getAdjacentVertices(currentVertex)){
            int distance = distanceTable.get(currentVertex).getDistance() +
                    graph.getWeightedEdge(currentVertex, neighbour);

            // the neighbour == source && distanceTable.get(neighbour).getDistance() == 0 )
            // ensure when return to source, the proper distance is updated, this only happens at the
            // 1st time it passes to the source. This is due to the initialization of source distance to 0
            if (distanceTable.get(neighbour).getDistance() > distance ||
                    (neighbour == source && distanceTable.get(neighbour).getDistance() == 0 )){
                distanceTable.get(neighbour).setDistance(distance);
                distanceTable.get(neighbour).setLastVertex(currentVertex);

                VertexInfo neighbourVertexInfo = vertexInfoMap.get(neighbour);
                if (neighbourVertexInfo != null){
                    queue.remove(neighbourVertexInfo);
                }

                neighbourVertexInfo = new VertexInfo(neighbour, distance);
                queue.add(neighbourVertexInfo);
                vertexInfoMap.put(neighbour,neighbourVertexInfo);
            }
        }
    }
    return distanceTable;
}

    /**
     * ShortestPath using Dijkstra Algorithm
     * @param graph
     * @param source
     * @param destination
     * @return
     */
    public static int shortestPath(Graph graph, Integer source, Integer destination){
    if (source >= graph.getNumVerticess() || source < 0
            || destination >= graph.getNumVerticess() || destination <0){
        return 0;
    }
    Map<Integer, DistanceInfo> distanceTable = buildDistanceTable(graph,source);

    Stack<Integer> stack = new Stack<>();
    stack.push(destination);

    int result = 0;
    int previousVertex = distanceTable.get(destination).getLastVertex();

    while(previousVertex != -1 && previousVertex != source){
        stack.push(previousVertex);
        previousVertex = distanceTable.get(previousVertex).getLastVertex();
    }

    int current = source;
    if (previousVertex == -1) {
     //   System.out.println("There is no path from node: "
     //           + source + " to node: " + destination);
        return result;

    } else {
        //System.out.print ("Smallest Path is " + source );
        while (!stack.isEmpty()){
            int next = stack.pop();
            result += graph.getWeightedEdge(current, next);
            current = next;
            //System.out.print(" -> " + next);
        }
    }

    return result;
}

    /**
     * Calculate the distance on a path
     *
     * @param graph
     * @param path
     * @return distance on a path, 0 means no such path
     */
    public static int distanceOnPath(Graph graph, List<Integer> path){
    int result = 0;
    if (path == null || path.size() <2 ){
        return 0;
    }
    int currentVertex = path.get(0);

    for (int j = 1; j < path.size(); j++){
        int nextVertex = path.get(j);
        if (graph.getWeightedEdge(currentVertex,nextVertex) != 0){
            result +=graph.getWeightedEdge(currentVertex,nextVertex);
            currentVertex = nextVertex;
        } else {
            return 0;
        }
    }

    return result;
}

    /**
     * Generate the list of visiting VertexIndex based on city name
     * @param cityNameMap
     * @param path
     * @return
     */
    public static List<Integer> generatePathList (HashMap<Character, Integer>cityNameMap, List<Character> path){
        List<Integer> pathList = new ArrayList<>();

        if (path == null || path.size() == 0){
            return null;
        }

        for (int i = 0; i< path.size(); i++){
            // in case of invalid city
            if (!cityNameMap.containsKey(path.get(i))){
                return null;
            }
            pathList.add(cityNameMap.get(path.get(i)));
        }
        return pathList;
    }

    /**
     * find the total path between two nodes with exactly numStops edge
     * @param graph
     * @param source
     * @param destination
     * @param numStops
     * @return
     */
    public static int bfsWithSrcAndDestUnweighted(Graph graph, int source, int destination, int numStops) {
        if (source >= graph.getNumVerticess() || source < 0
                || destination >= graph.getNumVerticess() || destination <0){
            return 0;
        }
        Deque<VertexInfo> queue = new ArrayDeque<>();
        queue.addLast(new VertexInfo(source, 0 ));

        // stores the total numbers of paths from source to destination having numStops edge
        int count = 0;

        while (!queue.isEmpty()) {
            VertexInfo vertexInfo = queue.removeFirst();

            int vertex = vertexInfo.getVertexId();
            int depth = vertexInfo.getDistance();
            if (vertex == destination && depth == numStops){
                count++;
            }
            if (depth > numStops){
                break;
            }
            List<Integer> list = graph.getAdjacentVertices(vertex);
            for (int v : list) {
                queue.addLast(new VertexInfo(v, depth+1));
            }
        }
        return count;
    }

    /**
     * Find the total # of path between two nodes with total cost less than weight
     * @param graph
     * @param source
     * @param destination
     * @param weight
     * @return
     */
    public static int bfsWithSrcAndDestWeighted(Graph graph, int source, int destination, int weight) {
        if (source >= graph.getNumVerticess() || source < 0
                || destination >= graph.getNumVerticess() || destination <0){
            return 0;
        }
        Deque<VertexInfo> queue = new ArrayDeque<>();
        queue.addLast(new VertexInfo(source, 0 ));

        // stores the total numbers of paths from source to destination have total path weight less than weight
        int count = 0;

        while (!queue.isEmpty()) {
            VertexInfo vertexInfo = queue.removeFirst();

            int vertex = vertexInfo.getVertexId();
            int distance = vertexInfo.getDistance();

            if (distance >= weight){
                continue;
            }
            if (vertex == destination && distance !=0){
                count++;
            }

            List<Integer> list = graph.getAdjacentVertices(vertex);
            for (int v : list) {
                queue.addLast(new VertexInfo(v, distance+ graph.getWeightedEdge(vertex,v)));
            }
        }
        return count;
    }

}
