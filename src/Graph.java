import java.util.List;

public interface Graph {

    enum GraphType{
        DIRECTED,
        UNDIRECTED
    }

    void addEdge(int v1, int v2, int weight);

    List<Integer> getAdjacentVertices(int v);

    int getIndegree(int v);

    int getNumVerticess();

    int getWeightedEdge (int v1, int v2);

}

