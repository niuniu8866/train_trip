import java.util.HashMap;
import java.util.List;

/**
 * a wrapper class for Graph and its cityNameMap
 */
public class GraphInfo {
    private Graph graph;
    private HashMap<Character, Integer> cityNameMap;

    public GraphInfo(Graph graph, HashMap<Character, Integer> cityNameMap) {
        this.graph = graph;
        this.cityNameMap = cityNameMap;
    }

    public GraphInfo(List<String> input){

        HashMap<Character, Integer> cityNameMap = new HashMap<>();
        int vertexIndex = 0;
        for (String str: input){
            str = str.trim();
            char cityFrom = str.charAt(0);
            char cityTo = str.charAt(1);
            if (!cityNameMap.containsKey(cityFrom)){
                cityNameMap.put(cityFrom,vertexIndex);
                vertexIndex++;
            }
            if (!cityNameMap.containsKey(cityTo)){
                cityNameMap.put(cityTo,vertexIndex);
                vertexIndex++;
            }
        }
        int numVertex = cityNameMap.size();

        Graph graph = new AdjacencyMatrixGraph(numVertex, Graph.GraphType.DIRECTED);
        for (String str: input){
            str = str.trim();
            int cityFrom = cityNameMap.get(str.charAt(0));
            int cityTo = cityNameMap.get(str.charAt(1));
            int weight = Integer.parseInt(str.substring(2));
            graph.addEdge(cityFrom,cityTo,weight);
        }

        this.cityNameMap = cityNameMap;
        this.graph = graph;
    }

    public Graph getGraph() {
        return graph;
    }

    public HashMap<Character, Integer> getCityNameMap() {
        return cityNameMap;
    }
}
